class Dialog {
    fun showTitle() = println("Título")
    fun setTitle(text: String) = println("Texto del Título: $text")
    fun setTitleColor(color: String) = println("Color del Título: $color")
    fun showMessage() = println("Mostrando mensaje")
    fun setMessage(text: String) = println("Mensaje: $text")
    fun setMessageColor(color: String) = println("Color del mensaje: $color")
    fun show() = println("Mostrando diálogo: $this")
}