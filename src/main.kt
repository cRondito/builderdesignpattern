import java.io.File

fun main(args: Array<String>) {
    val dialog: Dialog = dialog {
        title {
            text = "Titulo del mensaje"
            color = "#ff0000 - Rojo"
        }
        message {
            text = "Texto del mensaje"
            color = "#FFFFFF - Negro"
        }
    }

    dialog.show()
}

fun dialog(init: DialogBuilder.() -> Unit): Dialog {
    return DialogBuilder(init).build()
}
